#ifndef LEG_H
#define LEG_H

#include <ros/ros.h>

#include <gazebo_msgs/ContactsState.h>

namespace LegTypes
{
enum LegType
{
  FrontLeft = 1,
  FrontRight,
  RearLeft,
  RearRight,
  Empty
};
}
typedef LegTypes::LegType LegType;

struct JointPosition
{
  double hip;
  double knee;
};

struct Position
{
  double x;
  double y;
};

struct Parameters
{
  double l1;
  double l2;
};

struct State
{
  JointPosition angles;
  Position coordinates;
};

class Leg
{
public:
  Leg(LegType leg_type, int current_time, ros::NodeHandle &nh);

  LegType getLegType() const;

  void doStep();
  void setCurrentAnglesAndRefreshPosition(JointPosition joint_position);

private:
  LegType leg_type_;
  bool is_contact_;
  bool is_descend_phase_;
  bool is_new_y_set_;
  float total_force_;
  float new_y_coordinate;
  float new_x_coordinate;
  float x_decrementer;
  int force_measurement_count_;
  bool is_front_leg_;
  Parameters geometry_;
  State current_state_;
  State set_state_;
  int current_time_;
  Position position_table_[400];
  ros::NodeHandle nh_;
  ros::Subscriber contact_sub_;
  ros::Publisher hip_pub_;
  ros::Publisher knee_pub_;
  ros::Publisher ankle_pub_;

  //Methods
  void calculateDefaultPositionTable();
  void calculateLinearPathFromResolutionStartStop(int offset, int length, Position start, Position stop);
  void getCoordinatesForTime();
  void checkContactState();
  void calculateIK();
  void calculateFK();
  void publishMessages();

  //Subscriber callback
  void contactsStateCallback(const gazebo_msgs::ContactsStatePtr &msg);
};

#endif // LEG_H
