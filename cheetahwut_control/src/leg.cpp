#include <leg.h>

#include <cmath>
#include <ros/ros.h>
#include <std_msgs/Float64.h>
#include <boost/lexical_cast.hpp>

#define FRONT_L1 0.2
#define FRONT_L2 0.18
#define REAR_L1 0.14
#define REAR_L2 0.12
#define REAR_L3 0.12

#define HIP_MIN -1.5
#define HIP_MAX 2

#define KNEE_MIN 0.4
#define KNEE_MAX 2.5

#define MAX_REACH 0.36
#define MIN_REACH 0.12

#define CONTACT_TRESHOLD 35
#define MEASUREMENT_COUNT 1

#define REAR_BOTTOM_X -0.1
#define REAR_TOP_X -0.05
#define FRONT_BOTTOM_X 0.1
#define FRONT_TOP_X 0.05
#define FRONT_MIN_Y 0.24
#define FRONT_MAX_Y 0.32
#define REAR_MIN_Y 0.24
#define REAR_MAX_Y 0.32

Leg::Leg(LegType leg_type, int current_time, ros::NodeHandle &nh) :
  leg_type_(leg_type), current_time_(current_time), nh_(nh)
{
  //helper value - shortens various expressions
  is_front_leg_ = (leg_type_ == LegTypes::FrontLeft || leg_type_ == LegTypes::FrontRight);

  //helper value used in publisher/subscriber topics
  std::string leg_number = boost::lexical_cast<std::string>(int(leg_type_));

  //Initialize publishers
  hip_pub_ = nh_.advertise<std_msgs::Float64>("/cheetahwut/hip"+leg_number+"_position_controller/command", 1000);
  knee_pub_ = nh_.advertise<std_msgs::Float64>("/cheetahwut/knee"+leg_number+"_position_controller/command", 1000);
  if (!is_front_leg_)
  {
    ankle_pub_ = nh_.advertise<std_msgs::Float64>("/cheetahwut/ankle"+leg_number+"_position_controller/command", 1000);
  }

  //Initialize subscribers
  contact_sub_ = nh_.subscribe("/cheetahwut/bumper_"+leg_number+"_state", 1, &Leg::contactsStateCallback, this);

  //Initialize ff values
  is_new_y_set_ = false;
  new_y_coordinate = 0;
  is_descend_phase_ = false;
  total_force_ = 0;
  force_measurement_count_ = 0;

  //set geometry
  if (is_front_leg_)
  {
    geometry_.l1 = FRONT_L1;
    geometry_.l2 = FRONT_L2;
  }
  else
  {
    geometry_.l1 = REAR_L1 + REAR_L3;
    geometry_.l2 = REAR_L2;
  }

  calculateDefaultPositionTable();
}

void Leg::contactsStateCallback(const gazebo_msgs::ContactsStatePtr &msg)
{
  if (!msg->states.empty()) {
    geometry_msgs::Vector3 tmp = ((gazebo_msgs::ContactState) msg->states.at(0)).total_wrench.force;
    total_force_ += sqrt(pow(tmp.x, 2) + pow(tmp.y, 2) + pow(tmp.z, 2));
  }

  force_measurement_count_++;

}

void Leg::checkContactState()
{
  if (force_measurement_count_ == MEASUREMENT_COUNT) {
    total_force_ /= MEASUREMENT_COUNT;
    if (leg_type_ == LegTypes::FrontRight && 0)
      printf("%lf\n", total_force_);

    if (total_force_ > CONTACT_TRESHOLD)
    {
      is_contact_ = true;
    }
    else
    {
      is_contact_ = false;
    }
    force_measurement_count_ = 0;
    total_force_ = 0;
  }
}

LegType Leg::getLegType() const
{
  return leg_type_;
}

void Leg::calculateLinearPathFromResolutionStartStop(int offset, int length, Position start, Position stop)
{
  double increment_y = (stop.y - start.y) / length;
  double increment_x = (stop.x - start.x) / length;

  for (int i = 0; i < length; i++)
  {
    position_table_[offset + i].x = start.x + (i * increment_x);
    position_table_[offset + i].y = start.y + (i * increment_y);
  }
}

void Leg::calculateDefaultPositionTable()
{
  /*
   * Assign extreme positions. The route is A->B->C->D->A etc.
   * Coordinates are defined on top.
   */
  Position A,B,C,D;
  A.x = REAR_BOTTOM_X;
  A.y = REAR_MAX_Y;
  B.x = REAR_TOP_X;
  B.y = REAR_MIN_Y;
  C.x = FRONT_TOP_X;
  C.y = FRONT_MIN_Y;
  D.x = FRONT_BOTTOM_X;
  D.y = FRONT_MAX_Y;

  //A->B
  calculateLinearPathFromResolutionStartStop(0, 15, A, B);
  //B->C
  calculateLinearPathFromResolutionStartStop(15, 20, B, C);
  //C->D
  calculateLinearPathFromResolutionStartStop(35, 15, C, D);
  //D->A
  calculateLinearPathFromResolutionStartStop(50, 350, D, A);
}

#define ENABLE_FORCE_FEEDBACK 1

void Leg::doStep()
{
  if (ENABLE_FORCE_FEEDBACK)
  {
    //reset y coordinate and contact state when lifting leg
    if (current_time_ == 0)
    {
      is_contact_ = false;
      is_new_y_set_ = false;
      new_y_coordinate = 0;
      new_x_coordinate = 0;
      x_decrementer = 0;
    }
    //set descend phase to check for contact
    if (current_time_ == 35)
    {
      is_descend_phase_ = true;
    }

    //"timeout" for checking contact, revert to default trajectory
    if (current_time_ == 100 && !is_new_y_set_)
    {
      is_new_y_set_ = true;
      new_y_coordinate = REAR_MAX_Y;
      is_descend_phase_ = false;
    }
    //refresh is_contact_ value
    checkContactState();

    //if descend phase check contact and calculate new y
    if (is_descend_phase_ && is_contact_)
    {
      is_new_y_set_ = true;
      is_descend_phase_ = false;

      //new coordinates
      new_y_coordinate = current_state_.coordinates.y;
      new_x_coordinate = current_state_.coordinates.x;

      x_decrementer = fabs(REAR_BOTTOM_X - new_x_coordinate)/(400 - current_time_);

      //recalculate position table for section 0(A)->15(B)
      Position A,B;
      A.x = REAR_BOTTOM_X;
      A.y = new_y_coordinate;
      B.x = REAR_TOP_X;
      B.y = REAR_MIN_Y;
      calculateLinearPathFromResolutionStartStop(0, 15, A, B);

      //ROS_INFO("Setting new y(%lf), old set y(%lf), step #(%d)\n", new_y_coordinate, set_state_.coordinates.y, current_time_);
      //ROS_INFO("Setting new x(%lf), old set x(%lf), step #(%d)\n", new_x_coordinate, set_state_.coordinates.x, current_time_);
    }
  }

  //gets x and y from position table and passes them to set_state_
  getCoordinatesForTime();

  //overwrite y set_state if new is set
  if (is_new_y_set_ && ENABLE_FORCE_FEEDBACK)
  {
    set_state_.coordinates.y = new_y_coordinate;
    if (new_x_coordinate)
    {
      set_state_.coordinates.x = new_x_coordinate;
      new_x_coordinate -= x_decrementer;
    }
  }


  //calculate joint coordinates from x and y and passes them to set_state_
  calculateIK();

  //publish them
  publishMessages();

  //increment timer
  ++current_time_ %= 400;
}

void Leg::setCurrentAnglesAndRefreshPosition(JointPosition joint_position)
{
  current_state_.angles = joint_position;
  if (is_front_leg_)
    current_state_.angles.hip *= -1;

  calculateFK();

  if (!is_front_leg_)
    current_state_.coordinates.x *= -1;

  if (leg_type_ == LegTypes::RearRight)
  {
    //printf("%lf, %lf\n", current_state_.angles.hip, current_state_.angles.knee);
    //printf("%lf, %lf\n", current_state_.coordinates.x, current_state_.coordinates.y);
  }
}

void Leg::calculateIK()
{
  double x,y, l1, l2;
  x = set_state_.coordinates.x;
  y = set_state_.coordinates.y;
  l1 = geometry_.l1;
  l2 = geometry_.l2;

  double beta = acos( (pow(x, 2) + pow(y, 2) - pow(l1, 2) - pow(l2, 2)) / (2 * l1 * l2) );

  double sigma = asin( (l2 * sin((is_front_leg_ ? 1 : -1) * beta)) / (sqrt(pow(x, 2) + pow(y, 2))) );
  double gamma = 0;

  if (x != 0)
  {
    gamma = atan2(y, x);
    //std::cout << "Gamma is: " << gamma * 180/M_PI << std::endl;
    gamma -= M_PI/2;
  }

  sigma += gamma;

  //delimiters
  if (beta < KNEE_MIN)
    beta = KNEE_MIN;
  else if (beta > KNEE_MAX)
    beta = KNEE_MAX;

  if (sigma < HIP_MIN)
    sigma = HIP_MIN;
  else if (sigma > HIP_MAX)
    sigma = HIP_MAX;

  set_state_.angles.knee = beta;
  set_state_.angles.hip = sigma;
}

void Leg::calculateFK()
{
  current_state_.coordinates.x = geometry_.l1 * sin(current_state_.angles.hip) + geometry_.l2 * sin(current_state_.angles.hip + current_state_.angles.knee);
  current_state_.coordinates.y = geometry_.l1 * cos(current_state_.angles.hip) + geometry_.l2 * cos(current_state_.angles.hip + current_state_.angles.knee);
}

void Leg::publishMessages()
{
  std_msgs::Float64 msg;

  msg.data = set_state_.angles.hip;
  hip_pub_.publish(msg);
  msg.data = set_state_.angles.knee;
  knee_pub_.publish(msg);

  if (!is_front_leg_)
  {
    ankle_pub_.publish(msg);
  }
}

void Leg::getCoordinatesForTime()
{
  set_state_.coordinates.x = position_table_[current_time_].x;
  set_state_.coordinates.y = position_table_[current_time_].y;
}
