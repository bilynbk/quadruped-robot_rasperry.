#include <jointstateclass.h>

#include <boost/foreach.hpp>

JointStateClass::JointStateClass()
{
}

JointStateClass::~JointStateClass()
{
}

void JointStateClass::jointControllerStateCallback(const sensor_msgs::JointStatePtr& msg)
{
  //parse incoming info
  /*
   * Order is:
   * name: ['ankle_leg_3', 'ankle_leg_4', 'hip_leg_1', 'hip_leg_2', 'hip_leg_3', 'hip_leg_4', 'knee_leg_1', 'knee_leg_2', 'knee_leg_3', 'knee_leg_4']   *
   * we don't care about ankle angle because it's not used in FK and it theoretically is equal to -knee_angle
   * Leg 1(0): 2, 6
   * Leg 2(1): 3, 7
   * Leg 3(2): 4, 8
   * Leg 4(3): 5, 9
   */

  for (int i = 0; i < 4; i++)
  {
    leg_angles_[i].hip = msg->position.at(2 + i);
    leg_angles_[i].knee = msg->position.at(6 + i);
  }
}

JointPosition JointStateClass::getAnglesForLegNumber(int number)
{
  return leg_angles_[number - 1];
}
